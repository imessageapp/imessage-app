<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use ApiBundle\Entity\ApiResponse;
use MainBundle\Entity\Interested;
use MainBundle\Entity\ProfileDetail;
use MainBundle\Entity\User;
use JMS\Serializer\SerializationContext;
use RedjanYm\FCMBundle\Entity\DeviceNotification;
use sngrl\PhpFirebaseCloudMessaging\Notification;

class BaseController extends FOSRestController
{

    protected function failure($code = 404, $message)
    {
        $output = new ApiResponse();
        $output->error = array('code' => $code, 'description' => $message);
        $output->success = false;
        $output->response = null;
        $view = $this->view($output);

        return $this->handleView($view);
    }


    protected function success($response, $context)
    {

        $output = new ApiResponse();
        $output->error = null;
        $output->success = true;
        $output->response = $response;
        $view = $this->view($output);
        $view = $view->setContext($view->getContext()->setGroups($context));

        return $this->handleView($view);

    }

    protected function validateTokenAndGetUser($token)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('MainBundle:User')->findOneBy(array('accessToken' => $token, 'locked' => false));


        return $user;
    }


    protected function getRandomNumber($random_string_length = 5)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVXYWZabcdefghijklmnopqrsuvtwxyz';
        $string = '';
        for ($i = 0; $i < $random_string_length; $i++) {
            $string .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $string;
    }

    public function convertTimeToMinutes($time)
    {
        $parts = explode(':', $time);
        $minutes = $parts[1];
        $hours = $parts[0];
        $total = $minutes + (60 * $hours);
        return $total;
    }

    public function convertDateToObject($date)
    {
        $dateObj = \DateTime::createFromFormat("d/m/Y", $date);

        return $dateObj;
    }

    public function validateField($key, $value)
    {
        $isValid = true;
        switch ($key) {
            case 'email': {
                if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    $isValid = false;
                }
                break;
            }
            case 'phone': {
                break;
            }
        }

        return $isValid;

    }

    /**
     * @param $data
     * @return bool|User|null|object
     */
    public function getFriendByIdOrPhone($data)
    {
        $em = $this->getDoctrine()->getManager();
        $friend = $em->getRepository('MainBundle:User')->findOneBy(['id' => $data]);

        if (!$friend) {
            $friend = $em->getRepository('MainBundle:User')->findOneBy(['phone' => $data]);

        }


        if (!$friend) {
            return false;
        }


        return $friend;


    }


}
