<?php

namespace ApiBundle\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use MainBundle\Entity\Contacts;
use MainBundle\Entity\User;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Delete;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\Annotations\View;

class ContactsController extends BaseController
{

    /**
     * @Get("/get",name="get_user_contact")
     * @ApiDoc(
     *  section = "Contacts",
     *  description="Get user Contacts",
     *
     *
     * headers={
     *     {
     *          "name" = "token",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "Token"
     *      }
     * }
     * )
     *
     *
     */
    public function getAction(Request $request)
    {

        $token = $request->headers->get('token', null);
        $user = $this->validateTokenAndGetUser($token);

        if (!$user) {
            return $this->failure(400, 'No Result Found');
        }

        return $this->success($user, array('Default', 'User','Contacts'));

    }


    /**
     * @Post("/uploads",name="user_contacts_uploads")
     * @ApiDoc(
     *  section = "Contacts",
     *  description="Upload Contacts",
     *   headers={
     *     {
     *          "name" = "token",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "Token"
     *      }
     * },
     *
     * parameters={
     *     {
     *          "name" = "contacts",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "Json String [{'name':'Temp','number':'9896098960'},{'name':'Dummy','number':'9896098960'}] "
     *      }
     *
     * }
     * )
     *
     *
     */
    public function uploadsAction(Request $request)
    {
        $json = $request->get('contacts', null);
        $token = $request->headers->get('token', null);
        $user = $this->validateTokenAndGetUser($token);

        if (!$user) {
            return $this->failure(400, 'Invalid Token');
        }
	//return $contacts:
       // $json = json_decode($contacts[0]);
	//return $json[0];
        if ($json == null) {
            if (json_last_error()) {
                return $this->failure(400,'json_code: '.json_last_error().' : '.json_last_error_msg());
            }
        }
        $em = $this->getDoctrine()->getManager();

        foreach ($json as $item) {
            try {
		$phoneNumber = $item['number'];
		$exist = $em->getRepository("MainBundle:Contacts")->findOneBy(['number'=> $phoneNumber,'user'=>$user->getId()]);
               if($exist) continue;
		$obj = new Contacts();
                $obj->setUser($user);
                $obj->setNumber($phoneNumber);
                $obj->setName($item['name']);
		$userAlreadyInApp = $em->getRepository("MainBundle:User")->findOneBy(['username'=> $phoneNumber]);
		if($userAlreadyInApp) $obj->setIsAppUser(1);
                $em->persist($obj);
                $em->flush();
            } catch (\Exception $e) {
                continue;
            }
        }

        return $this->success($user, array('Default', 'User','Contacts'));

    }


}
