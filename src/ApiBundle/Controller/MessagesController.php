<?php

namespace ApiBundle\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use MainBundle\Entity\Contacts;
use MainBundle\Entity\Message;
use MainBundle\Entity\Thread;
use MainBundle\Entity\User;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Delete;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\Annotations\View;

class MessagesController extends BaseController
{

    /**
     * @Get("/get",name="get_user_messages")
     * @ApiDoc(
     *  section = "Messages",
     *  description="Get user Messages",
     *
     *
     * headers={
     *     {
     *          "name" = "token",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "Token"
     *      }
     * },
     *
     * parameters={
     *     {
     *          "name" = "friend_id_or_phone",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "Friend Id/Phone Number"
     *      }
     *
     * }
     * )
     *
     *
     */
    public function getAction(Request $request)
    {

        $token = $request->headers->get('token', null);
        $friendIdOrPhone = $request->get('friend_id_or_phone', null);
        $user = $this->validateTokenAndGetUser($token);

        if (!$user) {
            return $this->failure(400, 'No Result Found');
        }


        $friend = $this->getFriendByIdOrPhone($friendIdOrPhone);

        if (!$friend) {
            return $this->failure(400, 'No Result Found');
        }


        $thread = $this->getDoctrine()->getManager()
            ->getRepository("MainBundle:Thread")->getThreads($user, $friend);

        return $this->success($thread, array('Default', 'User', 'Thread', 'Message'));

    }


    /**
     * @Post("/send",name="send_message_to_user")
     * @ApiDoc(
     *  section = "Messages",
     *  description="Send a Message",   
     *
     * parameters={
     *     {
     *          "name" = "friend_id_or_phone",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "Friend Id/Phone "
     *      },
     *     {
     *          "name" = "message",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "Messages Content"
     *      },
     *      {
     *          "name" = "file",
     *          "dataType" = "file",
     *          "requirement" = "\w+",
     *           "required"=false,
     *          "description" = "Media File[Optional]"
     *      },
     *      {
     *          "name" = "token",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "Token"
     *      },
     *      {
     *          "name" = "sent_at",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=false,
     *          "description" = "Message sent at"
     *      },
     *      {
     *          "name" = "type",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "Type can be audio,video or text"
     *      }
     *
     * }
     * )
     *
     *
     */
    public function sendAction(Request $request)
    {
        $messageBody = $request->get('message', null);
        $token = $request->get('token', null);
        $friendIdOrPhone = $request->get('friend_id_or_phone', null);
        $file = $request->files->get('file', null);
        $user = $this->validateTokenAndGetUser($token);
	$sent_at = $request->get('sent_at',null);
        $type = $request->get('type','text');

	if($sent_at){
		$sentAt = new \DateTime($sent_at);
	}else{ $sentAt = new \DateTime(); }

        if (!$user) {
            return $this->failure(400, 'Invalid Token');
        }


        $friend = $this->getFriendByIdOrPhone($friendIdOrPhone);

        if (!$friend) {
            return $this->failure(400, 'No Result Found');
        }
        if($user->getId() == $friend->getId()){
            return $this->failure(400, "Receiver and sender can't be the same");
        }
        

        if (empty($messageBody) and !empty($file)) {
            $messageBody = "File Added";
        }
        $em = $this->getDoctrine()->getManager();
        $isThreadAvailable = $em->getRepository("MainBundle:Thread")->findBySenderOrReceiver($user, $friend);


        if (!$isThreadAvailable) {
            $thread = new Thread();
            $thread->setCreatedBy($user);
            $thread->setReceiver($friend);
            $thread->setSender($user);
        } else {
            $thread = $isThreadAvailable;
            
        }
        
        $message = new Message();

        $message->setSender($user);
        $message->setReceiver($friend);
        $message->setBody($messageBody);
        $message->setSentAt($sentAt);
        $message->setThread($thread);
        $message->setType($type);

        $thread->addMessage($message);
        if ($file) {
            
            $this->get('systein_file_upload')->uploadMedia($file, $message);
        }
        $em->persist($thread);
        $em->flush();
        
        $data['messages'] = $message;

        return $this->success($data, array('Default', 'User', 'Thread'));

    }


    /**
     * @Post("/add/read-by",name="add_message_read")
     * @ApiDoc(
     *  section = "Messages",
     *  description="Add Message Is Read",
     *
     *
     * headers={
     *     {
     *          "name" = "token",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "Token"
     *      }
     * },
     *
     * parameters={
     *     {
     *          "name" = "thread_id",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "Thread Id"
     *      }
     *
     * }
     * )
     *
     *
     */
    public function addIsReadAction(Request $request)
    {

        $token = $request->headers->get('token', null);
        $thread_id = $request->get('thread_id', null);
        $user = $this->validateTokenAndGetUser($token);

        if (!$user) {
            return $this->failure(400, 'No Result Found');
        }


        $thread = $this->getDoctrine()->getManager()
            ->getRepository("MainBundle:Thread")->findOneBy(['id' => $thread_id]);
        $status = false;
        if ($thread) {

            if (count($thread->getMessages()) > 0) {
                /**
                 * @var $message Message
                 */
                $message = $thread->getMessages()->last();

                if ($message->getReceiver()->getId() == $user->getId()) {
                    $message->setIsRead(true);
                    $message->setReadAt(new \DateTime());
                    $this->getDoctrine()->getManager()->persist($message);
                    $this->getDoctrine()->getManager()->flush();
                    $status = true;
                }
            }

        }

        return $this->success($status, array('Default', 'User', 'Thread', 'Message'));

    }


}
