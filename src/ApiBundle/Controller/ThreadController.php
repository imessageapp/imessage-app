<?php

namespace ApiBundle\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use MainBundle\Entity\Contacts;
use MainBundle\Entity\Message;
use MainBundle\Entity\Thread;
use MainBundle\Entity\User;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Delete;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\Annotations\View;

class ThreadController extends BaseController
{

    /**
     * @Get("/get/by/user",name="get_user_threads")
     * @ApiDoc(
     *  section = "Threads",
     *  description="Get user Threads",
     *
     *
     * headers={
     *     {
     *          "name" = "token",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "Token"
     *      }
     * }
     *
     * )
     *
     *
     */
    public function getMyAction(Request $request)
    {

        $token = $request->headers->get('token', null);
        $user = $this->validateTokenAndGetUser($token);

        if (!$user) {
            return $this->failure(400, 'No Result Found');
        }


        $thread = $this->getDoctrine()->getManager()
            ->getRepository("MainBundle:Thread")->getMyThreads($user);

        return $this->success($thread, array('Default', 'User', 'Thread'));

    }


    /**
     * @Get("/get",name="get_a_thread")
     * @ApiDoc(
     *  section = "Threads",
     *  description="Get A  Thread by Id",
     *
     *
     * headers={
     *     {
     *          "name" = "token",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "Token"
     *      }
     * },
     * parameters = {
     *     {
     *          "name" = "thread_id",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "Thread Id"
     *      }
     * }
     *
     * )
     *
     *
     */
    public function getAction(Request $request)
    {

        $token = $request->headers->get('token', null);
        $user = $this->validateTokenAndGetUser($token);
        $id = $request->get('thread_id', null);

        if (!$user) {
            return $this->failure(400, 'No Result Found');
        }


        $thread = $this->getDoctrine()->getManager()
            ->getRepository("MainBundle:Thread")->findOneBy(['id' => $id]);

        return $this->success($thread, array('Default', 'User', 'Thread', 'Message'));

    }


}
