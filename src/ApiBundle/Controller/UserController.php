<?php

namespace ApiBundle\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use MainBundle\Entity\User;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Delete;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\Annotations\View;

class UserController extends BaseController
{

    /**
     * @Post("/profile",name="user_profile")
     * @ApiDoc(
     *  section = "User",
     *  description="Get user Profile",
     *
     *
     * headers={
     *     {
     *          "name" = "token",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "Token"
     *      }
     * }
     * )
     *
     *
     */
    public function getAction(Request $request)
    {

        $token = $request->headers->get('token', null);
        $user = $this->validateTokenAndGetUser($token);

        if (!$user) {
            return $this->failure(400, 'No Result Found');
        }

        return $this->success($user, array('Default', 'User'));

    }

    /**
     * @Delete("/delete",name="delete_account")
     * @ApiDoc(
     *  section = "User",
     *  description="Delete User Profile",
     *
     *
     * headers={
     *     {
     *          "name" = "token",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "Token"
     *      }
     * }
     * )
     *
     * @View(serializerGroups={"Default"})
     */
    public function deleteAction(Request $request)
    {

        $token = $request->get('token', null);
        $em = $this->getDoctrine()->getManager();

        $user = $this->validateTokenAndGetUser($token);

        if (!$user) {
            return $this->failure(400, 'Invalid Token');
        }

        $user->setEnabled(false);
        $user->setLocked(true);
        $em->persist($user);
        $em->flush();

        return $this->success("User Account Has Been Deleted", array('Default', "User"));
    }

    /**
     * @Post("/login",name="user_login")
     * @ApiDoc(
     *  section = "User",
     *  description="Login user",
     *
     * parameters={
     *     {
     *          "name" = "email",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "User Email/Phone/Username"
     *      },
     *   {
     *          "name" = "password",
     *          "dataType" = "string",
     *          "requirement" = "\w+",
     *           "required"=true,
     *          "description" = "password"
     *      }
     * }
     * )
     *
     *
     */
    public function loginAction(Request $request)
    {
        $email = $request->get('email', null);
        $password = $request->get('password', null);


        $um = $this->get('fos_user.user_manager');
        $user = $um->findUserByEmail($email); //find User By Email

        if (!$user) { //find User By Phone number
            $user = $um->findUserBy(array('phone' => $email, 'enabled' => true, 'locked' => false));
        }
        if ($user == null || $user->isEnabled() == false) {
            return $this->failure(401, "Email not registered/Account Disabled.");
        }
        if (!$this->checkUserPassword($user, $password)) {
            return $this->failure(401, "Password does not match.");
        }

        return $this->success($user, array('Default', 'User'));

    }

    protected function checkUserPassword(User $user, $password)
    {
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);

        if (!$encoder) {

            return false;
        }
        return $encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt());
    }

    /**
     * @Post("/register",name="signUp")
     * @ApiDoc(
     *  section = "User",
     *  description="user Sign Up",
     *  requirements={
     *
     *  {"name"="name","requirements"="\w+","dataType"="string","required"=true,"description"="Profile Name"},
     *  {"name"="email","requirements"="\w+","dataType"="string","required"=true,"description"="Email"},
     *  {"name"="password","requirements"="\w+","dataType"="string","required"=true,"description"="Password"},
     *  {"name"="phone","requirements"="\w+","dataType"="string","required"=false,"description"="Phone Number"},
     *  {"name"="ios_id","requirements"="\w+","dataType"="string","required"=false,"description"="IOS ID"},
     *
     *
     * },
     *
     * )
     *
     *
     */
    public function signUpAction(Request $request)
    {
        $name = $request->get('name', null);
        $email = $request->get('email', null);
        $password = $request->get('password', null);
        $phone = $request->get('phone', null);
        $ios = $request->get('ios_id', null);
        $em = $this->getDoctrine()->getManager();

        $hash = null;


        $user = $em->getRepository('MainBundle:User')->findOneBy(array('email' => $email));
        if ($user) {
            return $this->failure(400, "Email already exists");
        }
        $user = $em->getRepository('MainBundle:User')->findOneBy(array('phone' => $phone));
        if ($user) {
            return $this->failure(400, "Phone Number already exists");
        }

        if (!empty($name) && !empty($password) && !empty($email) && !empty($phone)) {

            $user = new User();


            $user->setUsername($phone);
            $user->setEmail($email);
            $user->setPhone($phone);
            $user->setName($name);
            $user->setEnabled(true);
            $user->setPlainPassword($password);
            $user->setIosId($ios);

            try {
                $em->persist($user);
                $em->flush();

                // check is in Contact List

                $isUserPhoneNumberInContacts = $em->getRepository('MainBundle:Contacts')->findOneBy(['number' => $phone]);

                if ($isUserPhoneNumberInContacts) {
                    $isUserPhoneNumberInContacts->setIsAppUser(true);
                    $em->persist($isUserPhoneNumberInContacts);
                    $em->flush();
                }

                return $this->success($user, array('Default', 'User'));

            } catch (\Symfony\Component\Config\Definition\Exception\Exception $e) {
                return $this->failure(404, $e->getMessage());
            } catch (UniqueConstraintViolationException $e) {
                return $this->failure(404, 'UserName/Email/Phone  already taken!!');
            }
        } else {
            return $this->failure(401, "All fields are mandatory");
        }
    }


    /**
     * @Post("/upload/media",name="upload_profile_media")
     * @ApiDoc(
     *  section = "User",
     *  description="Upload User Image",
     *  headers={
     *  {"name"="token","requirements"="\w+","dataType"="file","required"=true,"description"="User Token"}
     *     },
     *  parameters={
     *  {"name"="file","requirements"="\w+","dataType"="file","required"=true,"description"="Image"}
     * },
     *
     * )
     *
     *
     */
    public function uploaddMediaAction(Request $request)
    {
        $file = $request->files->get('file', null);
        $token = $request->headers->get('token', null);


        $em = $this->getDoctrine()->getManager();
        //Authenticate user here

        $user = $this->validateTokenAndGetUser($token);

        if (!$user) {
            return $this->failure(400, 'No Result Found');
        }

        if (empty($file)) {
            return $this->failure(400, 'Please Select An Image!!');
        }


        try {

            $res = $this->get('systein_file_upload')->uploadFile($file, 'dp');
            $user->setDp($res);
            $em->persist($user);
            $em->flush();
        } catch (\Exception $e) {
            return $this->failure(404, $e->getMessage());
        }

        return $this->success($user, array('Default', 'User'));
    }


}
