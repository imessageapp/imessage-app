<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;

/**
 * ApiResponse
 *
 * ORM\Entity
 * @ExclusionPolicy("all")
 */
class ApiResponse
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var array
     *
     * ORM\Column(name="error", type="array")
     * @Expose
     * @Groups({"Default","User","Thread","Message","Media","Contacts"})
     */
    public $error;



    /**
     * @var array
     *
     * ORM\Column(name="response", type="array")
     * @Expose
     * @Groups({"Default","User","Thread","Message","Media","Contacts"})
     */
    public $response;

    /**
     * @var string
     *
     * ORM\Column(name="response", type="string",length=255)
     * @Expose
     * @Groups({"Default","User","Thread","Message","Media","Contacts"})
     */
    public $success;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set error
     *
     * @param array $error
     *
     * @return ApiResponse
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * Get error
     *
     * @return array
     */
    public function getError()
    {
        return $this->error;
    }


    /**
     * Set response
     *
     * @param array $response
     *
     * @return ApiResponse
     */
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Get response
     *
     * @return array
     */
    public function getResponse()
    {
        return $this->response;
    }
}
