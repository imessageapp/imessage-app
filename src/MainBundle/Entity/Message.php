<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Accessor;

/**
 * Message
 *
 * @ORM\Table(name="message", options={"collate"="utf8mb4_bin"})
 * @ORM\Entity(repositoryClass="MainBundle\Repository\MessageRepository")
 * @ExclusionPolicy("all")
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"Default","User","Thread","Message"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text")
     * @Expose
     * @Groups({"Default","User","Thread","Message"})
     */
    private $body;


    /**
     * @var bool
     *
     * @ORM\Column(name="is_read", type="boolean")
     * @Expose
     * @Groups({"Default","User","Thread","Message"})
     */
    private $isRead;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;


    /** @var \DateTime
     *
     * @ORM\Column(name="sent_at", type="datetime",nullable=true)
     * @Expose
     * @Groups({"Default","User","Thread","Message"})
     */
    private $sentAt;

    /** @var \DateTime
     *
     * @ORM\Column(name="read_at", type="datetime",nullable=true)
     * @Expose
     * @Groups({"Default","User","Thread","Message"})
     */
    private $readAt;


    /**
     * @ORM\ManyToOne(targetEntity="Thread", inversedBy="messages")
     */
    private $thread;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @Expose
     * @Groups({"Default","User","Thread","Message"})
     */
    private $receiver;


    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @Expose
     * @Groups({"Default","User","Thread","Message"})
     */
    private $sender;

    /**
     * @ORM\OneToMany(targetEntity="Media", mappedBy="message")
     * @Expose
     * @Groups({"Default","User","Thread","Message"})
     */
    private $media;


    /**
     * @Accessor(getter="getSentAtAsString")
     * @Expose()
     * @Groups({"Default","User","Thread","Message"})
     */
    protected $sent_at_string;
    
    /**
     * @ORM\Column(name="type", type="string")
     * @Expose()
     * @Groups({"Default","User","Thread","Message"})
     */
    protected $type;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->isRead = 0;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Message
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Message
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set isRead
     *
     * @param boolean $isRead
     *
     * @return Message
     */
    public function setIsRead($isRead)
    {
        $this->isRead = $isRead;

        return $this;
    }

    /**
     * Get isRead
     *
     * @return boolean
     */
    public function getIsRead()
    {
        return $this->isRead;
    }

    /**
     * Set thread
     *
     * @param \MainBundle\Entity\Thread $thread
     *
     * @return Message
     */
    public function setThread(\MainBundle\Entity\Thread $thread = null)
    {
        $this->thread = $thread;

        return $this;
    }

    /**
     * Get thread
     *
     * @return \MainBundle\Entity\Thread
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * Set receiver
     *
     * @param \MainBundle\Entity\User $receiver
     *
     * @return Message
     */
    public function setReceiver(\MainBundle\Entity\User $receiver = null)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * Get receiver
     *
     * @return \MainBundle\Entity\User
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Set sender
     *
     * @param \MainBundle\Entity\User $sender
     *
     * @return Message
     */
    public function setSender(\MainBundle\Entity\User $sender = null)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return \MainBundle\Entity\User
     */
    public function getSender()
    {
        return $this->sender;
    }


    /**
     * Set sentAt
     *
     * @param \DateTime $sentAt
     *
     * @return Message
     */
    public function setSentAt($sentAt)
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    /**
     * Get sentAt
     *
     * @return \DateTime
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * Add medium
     *
     * @param \MainBundle\Entity\Media $medium
     *
     * @return Message
     */
    public function addMedia(\MainBundle\Entity\Media $medium)
    {
        $this->media[] = $medium;

        return $this;
    }

    /**
     * Remove medium
     *
     * @param \MainBundle\Entity\Media $medium
     */
    public function removeMedia(\MainBundle\Entity\Media $medium)
    {
        $this->media->removeElement($medium);
    }

    /**
     * Get media
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedia()
    {
        return $this->media;
    }


    public function __toString()
    {
        return $this->body ? $this->body : 'Message';
    }


    public function getSentAtAsString()
    {
        if (!$this->sentAt) {
            return "";
        }
        $dateTimeObj = $this->sentAt;
        return $dateTimeObj->format('h:i A');
                
    }


    /**
     * Set readAt
     *
     * @param \DateTime $readAt
     *
     * @return Message
     */
    public function setReadAt($readAt)
    {
        $this->readAt = $readAt;

        return $this;
    }

    /**
     * Get readAt
     *
     * @return \DateTime
     */
    public function getReadAt()
    {
        return $this->readAt;
    }
    
   

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Message
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
