<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Accessor;

/**
 * Thread
 *
 * @ORM\Table(name="thread")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\ThreadRepository")
 * @ExclusionPolicy("all")
 */
class Thread
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"Default","User","Thread","Message"})
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;




    /**
     * @var bool
     *
     * @ORM\Column(name="is_deleted", type="boolean")
     * @Expose
     * @Groups({"Default","User","Thread","Message"})
     */
    private $isDeleted;


    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="threads")
     * @Expose
     * @Groups({"Default","User","Thread","Message"})
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @Expose
     * @Groups({"Default","User","Thread","Message"})
     */
    private $receiver;


    /**
     * @ORM\ManyToOne(targetEntity="User")     
     * @Expose
     * @Groups({"Default","User","Thread","Message"})
     */
    private $sender;


    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="thread",cascade={"persist"})
     * @Expose
     * @Groups({"Default","User","Thread","Message"})
     * @ORM\OrderBy({"sentAt" = "ASC"})
     */
    private $messages;

    /**
     * @Accessor(getter="getLastMessage")
     * @Expose()
     * @Groups({"Default","User","Thread","Message"})
     */
    protected $last_message;



    /**
     * @Accessor(getter="getLastActivatedAt")
     * @Expose()
     * @Groups({"Default","User","Thread","Message"})
     */
    protected $last_activated_at;

    /**
     * @Accessor(getter="getSentAt")
     * @Expose()
     * @Groups({"Default","User","Thread","Message"})
     */
    protected $sent_at;





    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->isDeleted = false;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Thread
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     *
     * @return Thread
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return bool
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set createdBy
     *
     * @param \MainBundle\Entity\User $createdBy
     *
     * @return Thread
     */
    public function setCreatedBy(\MainBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \MainBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set receiver
     *
     * @param \MainBundle\Entity\User $receiver
     *
     * @return Thread
     */
    public function setReceiver(\MainBundle\Entity\User $receiver = null)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * Get receiver
     *
     * @return \MainBundle\Entity\User
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Set sender
     *
     * @param \MainBundle\Entity\User $sender
     *
     * @return Thread
     */
    public function setSender(\MainBundle\Entity\User $sender = null)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return \MainBundle\Entity\User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Add message
     *
     * @param \MainBundle\Entity\Message $message
     *
     * @return Thread
     */
    public function addMessage(\MainBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \MainBundle\Entity\Message $message
     */
    public function removeMessage(\MainBundle\Entity\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }


    public function getLastMessage() {
        if (count($this->getMessages()) == 0) {
            return "";
        }
        if ($this->getMessages()->last()) {
            return $this->getMessages()->last()->getBody();
        } else {
            return "";
        }
    }

    public function getLastActivatedAt() {
        if (count($this->getMessages()) == 0) {
            return "";
        }
        $diff = date_diff(new \DateTime(), $this->getMessages()->last()->getCreatedAt());
        //var_dump( $diff ); die;
        if ($diff->y) {
            if ($diff->y == 1) {
                return $diff->y . " year ago";
            } else {
                return $diff->y . " years ago";
            }
        }
        if ($diff->m) {
            if ($diff->m == 1) {
                return $diff->m . " month ago";
            } else {
                return $diff->m . " months ago";
            }
        }
        if ($diff->d > 7 && $diff->d < 14) {
            return "1" . " week ago";
        }
        if ($diff->d > 14 && $diff->d < 21) {
            return "2" . " weeks ago";
        }
        if ($diff->d) {
            if ($diff->d == 1) {
                return $diff->d . " day ago";
            } else {
                return $diff->d . " days ago";
            }
        }
        if ($diff->h) {
            if ($diff->h == 1) {
                return $diff->h . " hour ago";
            } else {
                return $diff->h . " hours ago";
            }
        }
        if ($diff->i) {
            if ($diff->i == 1) {
                return $diff->i . " minute ago";
            } else {
                return $diff->i . " minutes ago";
            }
        }
        if ($diff->s > 5) {
            return $diff->s . " seconds ago";
        } else {
            return 'just now';
        }
    }


    public function getSentAt() {
        if (!$this->getMessages()) {
            return "";
        }

        return $this->getLastMessage();
    }

}
