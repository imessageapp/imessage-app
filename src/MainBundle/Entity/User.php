<?php

namespace MainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use MainBundle\Service\UrlService;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * User
 *
 * @ORM\Entity(repositoryClass="MainBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     * @Groups({"User"})
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name",type="string", length=20,nullable=true)
     * @Expose
     * @Groups({"User"})
     */
    private $name;


    /**
     * @var string
     * @ORM\Column(name="phone",type="string", length=20,nullable=true)
     * @Expose
     * @Groups({"User"})
     */
    private $phone;


    /**
     * @var bool
     *
     * @ORM\Column(name="locked", type="boolean", nullable=true)
     * @Expose
     * @Groups({"User"})
     */
    private $locked = false;


    /**
     * @var string
     * @ORM\Column(name="dp",type="string", length=255,nullable=true)
     *
     */
    private $dp;


    /**
     * @var \DateTime
     * @ORM\Column(name="created_at",type="datetime", nullable=false)
     * @Expose
     * @Groups({"User"})
     */
    private $createdAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_notification_on", type="boolean",options={"default":true})
     * @Expose
     * @Groups({"Default"})
     */
    private $isNotificationOn = true;


    /**
     * @var string
     * @ORM\Column(name="ios_id", type="string", length=255, nullable=true)
     * @Expose
     * @SerializedName("ios_id")
     * @Groups({"Default"})
     */
    protected $iosId = false;

    /**
     * @var string
     * @ORM\Column(name="access_token",type="string", length=255)
     * @Expose
     * @Groups({"User"})
     */
    private $accessToken;


    /**
     * @ORM\OneToMany(targetEntity="Thread", mappedBy="createdBy")
     */
    private $threads;

    /**
     * @ORM\OneToMany(targetEntity="Contacts", mappedBy="user")
     * @Expose
     * @Groups({"Contacts"})
     * @ORM\OrderBy({"isAppUser" = "DESC"})
     */
    private $contacts;


    private $file;


    /**
     * @Accessor(getter="getDpUrl")
     * @SerializedName("dp")
     * @Expose
     * @Groups({"Default","User"})
     *
     */
    public $dpUrl = false;


    public function __construct()
    {
        parent::__construct();

        $this->accessToken = md5(time() . rand(0, 1000) . 'imessage-app' . uniqid('imessages'));

        $this->createdAt = new \DateTime();


    }


    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        if ($file) {
            $this->file = $file;
            $this->dp = md5(time()) . "." . $this->getFile()->guessExtension();
        }

    }


    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }



    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        $object = new UrlService();
        if ($this->getFile()) {
            $this->getFile()->move(
                $object->getUploadDir('DP'), $this->dp);

            $this->setFile(null);
        }

    }


    /* End File Upload*/

    /**
     * @return string
     */
    public function getDpUrl()
    {

        $object = new UrlService();
        $baseUrl = $object->getBaseUrl();
        if ($this->getDp()) {

            return $baseUrl . $object::getUploadsDirectory('dp') . '/' . $this->getDp();
        }
        return $object->getDefaultImage();

    }

    public function __toString()
    {
        return $this->name ? $this->name . '(' . $this->email . ')' : $this->email;
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set locked
     *
     * @param boolean $locked
     *
     * @return User
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Set dp
     *
     * @param string $dp
     *
     * @return User
     */
    public function setDp($dp)
    {
        $this->dp = $dp;

        return $this;
    }

    /**
     * Get dp
     *
     * @return string
     */
    public function getDp()
    {
        return $this->dp;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set isNotificationOn
     *
     * @param boolean $isNotificationOn
     *
     * @return User
     */
    public function setIsNotificationOn($isNotificationOn)
    {
        $this->isNotificationOn = $isNotificationOn;

        return $this;
    }

    /**
     * Get isNotificationOn
     *
     * @return boolean
     */
    public function getIsNotificationOn()
    {
        return $this->isNotificationOn;
    }

    /**
     * Set iosId
     *
     * @param string $iosId
     *
     * @return User
     */
    public function setIosId($iosId)
    {
        $this->iosId = $iosId;

        return $this;
    }

    /**
     * Get iosId
     *
     * @return string
     */
    public function getIosId()
    {
        return $this->iosId;
    }

    /**
     * Set accessToken
     *
     * @param string $accessToken
     *
     * @return User
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * Get accessToken
     *
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * Add thread
     *
     * @param \MainBundle\Entity\Thread $thread
     *
     * @return User
     */
    public function addThread(\MainBundle\Entity\Thread $thread)
    {
        $this->threads[] = $thread;

        return $this;
    }

    /**
     * Remove thread
     *
     * @param \MainBundle\Entity\Thread $thread
     */
    public function removeThread(\MainBundle\Entity\Thread $thread)
    {
        $this->threads->removeElement($thread);
    }

    /**
     * Get threads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getThreads()
    {
        return $this->threads;
    }

    /**
     * Add contact
     *
     * @param \MainBundle\Entity\Contacts $contact
     *
     * @return User
     */
    public function addContact(\MainBundle\Entity\Contacts $contact)
    {
        $this->contacts[] = $contact;

        return $this;
    }

    /**
     * Remove contact
     *
     * @param \MainBundle\Entity\Contacts $contact
     */
    public function removeContact(\MainBundle\Entity\Contacts $contact)
    {
        $this->contacts->removeElement($contact);
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContacts()
    {
        return $this->contacts;
    }
}
