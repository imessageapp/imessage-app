<?php

namespace MainBundle\Repository;

use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

/**
 * UserRepository
 *
 * This class was generated by the PhpStorm "Php Annotations" Plugin. Add your own custom
 * repository methods below.
 */
class UserRepository extends EntityRepository
{
    public function searchBy($education, $name, $height, $age, $caste, $familyType)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        $qb->select('u')
            ->from($this->_entityName, 'u')
            ->leftJoin('u.profileDetail', 'pd')
            ->leftJoin('u.familyDetail', 'fd')
            ->leftJoin('u.detailedProfile', 'detailed_profile')
            ->leftJoin('u.partnerPreferences', 'partner_preference')
            ->where('u.enabled = :bool AND u.isApproved = :bool')
            ->andWhere('u.locked = :NA AND u.isRejectedByAdmin = :NA')
            ->setParameters(['bool' => true, 'NA' => false]);
        if ($name) {
            $qb->andWhere('u.name LIKE LOWER(:Name) OR u.email LIKE LOWER(:Name)')
                ->setParameter('Name', "%$name%");

        }
        if ($education) {
            $qb->andWhere('pd.education LIKE :Education')
                ->setParameter('Education', "$education%");

        }
        if ($height) {
            $qb->andWhere('pd.height LIKE :Height')
                ->setParameter('Height', "%$height%");

        }
        if ($age) {
            $qb->andWhere('pd.age = :Age')
                ->setParameter('Age', "$age");

        }
        if ($caste) {
            $qb->andWhere('pd.caste LIKE :Caste')
                ->setParameter('Caste', "%$caste%");

        }
        if ($familyType) {
            $qb->andWhere('fd.familyType LIKE  :FamilyType')
                ->setParameter('FamilyType', "%$familyType%");

        }

        $profiles = $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true)
            ->getResult();

        return $profiles;
    }


    public function getUserByRole($role)
    {

        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder();

        try {
            $user = $qb->select('u')
                ->from($this->_entityName, 'u')
                ->where('u.roles LIKE :roles')
                ->setParameter('roles', "%" . $role . "%")
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }


        return $user;
    }

}
