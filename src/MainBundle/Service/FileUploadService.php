<?php

namespace MainBundle\Service;


use MainBundle\Entity\Media;
use MainBundle\Entity\Message;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;

class FileUploadService
{

    public $em;
    public $service;
    public $route;


    public function __construct(Container $container)
    {
        $this->service = $container;
        $this->em = $this->service->get('doctrine.orm.entity_manager');
    }

    /**
     * @param File $file
     * @param $dir
     * @return string
     */
    public function uploadFile(File $file, $dir)
    {
        try {
            $targetDirectory = UrlService::getUploadsDirectory($dir);

            //generate unique filename
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            //move the file
            $file->move($targetDirectory, $fileName);

            return $fileName;
        } catch (FileException $e) {
            return $e->getMessage();
        }

    }


    public function uploadMedia(File $file, Message $message)
    {
        
        try {
            $targetDirectory = UrlService::getUploadsDirectory('MEDIA');
            
            $extension = "mp3";
            if(empty($file->guessExtension())){
                $fileName = md5(uniqid()) . '.mp3' ;
            }
            else{
                //generate unique filename
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();  
                $extension = $file->guessExtension();
            }
            
            $obj = new Media();
            $obj->setName($fileName);
            $obj->setExtension('.'.$extension);
            $obj->setSize($file->getSize());
            $obj->setMessage($message);

            $em = $this->service->get('doctrine.orm.entity_manager');
            $file->move($targetDirectory, $fileName);
            $message->addMedia($obj);
            $em->persist($obj);
            $em->flush();
            

            return $obj;
        } catch (FileException $e) {          
            return $e->getMessage();
        } catch (\Exception $e) {           
            return $e->getMessage();
        }


    }


}