<?php

namespace MainBundle\Service;

class UrlService
{

    public function getContainerInstance()
    {
        global $kernel;

        if ('AppCache' == get_class($kernel)) {
            $kernel = $kernel->getKernel();
        }
        return $kernel->getContainer();
    }

    public function getBaseUrl()
    {


        $request = $this->getContainerInstance()->get('request_stack')->getCurrentRequest();
        $url = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        return $url . '/';
    }

    public function getExtension($name)
    {

        $temp = explode('.', $name);
        if (count($temp) > 0) {
            return $temp[1];
        } else {
            return null;
        }
    }


    public function getDefaultImage()
    {
        return $this->getBaseUrl() . 'default.png';
    }

    /**
     * @param $key
     * @return string
     */
    static function getUploadsDirectory($key)
    {

        $key = strtolower($key);
        switch ($key) {
            case 'gallery': {
                return 'uploads/media/users/gallery';
                break;
            }
            case 'dp': {
                return 'uploads/media/users/dp';
                break;
            }
            case 'media': {
                return 'uploads/media/messages';
                break;
            }
            default: {
                return 'uploads/media/users/images';
                break;
            }
        }

    }


    public function getUploadDir($key)
    {
        $key = strtolower($key);
        switch ($key) {
            case 'gallery': {
                return 'uploads/media/users/gallery';
                break;
            }
            case 'dp': {
                return 'uploads/media/users/dp';
                break;
            }
            case 'media': {
                return 'uploads/media/messages';
                break;
            }
            default: {
                return 'uploads/media/users/images';
                break;
            }
        }


    }


}
